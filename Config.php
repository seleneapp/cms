<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms;

use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\Package\PackageConfiguration;
use \Selene\Module\Config\Validator\Nodes\RootNode;
use \Selene\Module\Routing\RouteBuilder;
use \Selene\Module\Routing\RouteCollection;

/**
 * @class Config
 * @package Selene\Package\Cms
 * @version $Id$
 */
class Config extends PackageConfiguration
{
    public function setup(BuilderInterface $builder, array $config)
    {
        var_dump('CMS');
        $this->setupRoutes($builder, $config['uri']);
    }

    /**
     * setupRoutes
     *
     * @param BuilderInterface $containerBuilder
     * @param mixed $uri
     *
     * @access private
     * @return mixed
     */
    private function setupRoutes(BuilderInterface $containerBuilder, $uri)
    {
        $routes = $containerBuilder->getContainer()->get('routing.routes');
        $childRoutes = new RouteCollection;

        $loader = $this->getRoutingLoader($containerBuilder, $routes);

        $loader->load(function ($routes) use ($uri, $containerBuilder) {
            $this->nestRoutes($routes, $containerBuilder, $uri);
        });
    }

    /**
     * nestRoutes
     *
     * @param RouteBuilder $routes
     * @param BuilderInterface $builder
     * @param string $uri
     *
     * @return void
     */
    private function nestRoutes(RouteBuilder $routes, BuilderInterface $builder, $uri)
    {
        $loader = $this->getRoutingLoader($builder, $rc = new RouteCollection);
        $loader->load('routing.xml');

        $routes->group($uri, $this->getRouteRequirements());
        $routes->appendRoutes($rc);
        $routes->endGroup();
    }

    protected function getRouteRequirements()
    {
        return [
            'before' => 'auth'
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getResources()
    {
        return ['services.xml'];
    }

    /**
     * {@inheritdoc}
     */
    public function getConfigTree(RootNode $rootNode)
    {
        $alias = $this->getPackageAlias();
        $rootNode
            ->string('uri')
                ->defaultValue('admin')
                ->notEmpty()
                ->end();
    }
}
