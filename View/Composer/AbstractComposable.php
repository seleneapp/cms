<?php

/**
 * This File is part of the Selene\Package\Cms\View\Composer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View\Composer;

use \Selene\Module\View\Composer\Context;
use \Selene\Module\View\Composer\Composable;

/**
 * @class AbstractComposable
 * @package Selene\Package\Cms\View\Composer
 * @version $Id$
 */
abstract class AbstractComposable implements Composable
{
    public function compose(Context $context)
    {
        $context->withContext($this->getContext());
    }

    abstract protected function getContext();
}
