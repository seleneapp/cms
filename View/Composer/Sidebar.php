<?php

/**
 * This File is part of the Selene\Package\Cms\View\Composer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View\Composer;

use \Selene\Module\View\Composer\Context;
use \Selene\Module\View\Composer\Composable;

/**
 * @class Menu
 * @package Selene\Package\Cms\View\Composer
 * @version $Id$
 */
class Sidebar implements Composable
{
    public function compose(Context $context)
    {
        $context
            ->withContext($this->getContext())
            ->nestView('icon', 'cms:partials:icon.twig', ['icon' => 'icon name']);
    }

    private function getContext()
    {
        return [
            'sections' => [
                [
                    'title' => 'Settings',
                    'name'  => 'settings',
                    'items' => [1, 2, 3]
                ],
                [
                    'title' => 'Whatever',
                    'name'  => 'whatever',
                    'items' => [4, 5, 6]
                ]
            ]
        ];
    }
}
