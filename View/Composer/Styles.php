<?php

/**
 * This File is part of the Selene\Package\Cms\View\Composer package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\View\Composer;

use \Selene\Module\View\Composer\Context;
use \Selene\Module\View\Composer\Composable;

/**
 * @class Menu
 * @package Selene\Package\Cms\View\Composer
 * @version $Id$
 */
class Styles extends AbstractComposable
{
    /**
     * {@inheritdoc}
     */
    protected function getContext()
    {
        return [
            'styles' => [
                'package/cms/assets/css/main.css'
            ]
        ];
    }
}
