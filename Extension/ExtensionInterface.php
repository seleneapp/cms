<?php

/**
 * This File is part of the Selene\Package\Cms\Extension package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\Package\PackageInterface;

/**
 * @class ExtensionInterface
 * @package Selene\Package\Cms\Extension
 * @version $Id$
 */
interface ExtensionInterface extends PackageInterface
{
    const T_BACKEND  = 34677;

    const T_FRONTEND = 34678;

    public function getType();
}
