<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Extension;

use \Selene\Module\Events\DispatcherInterface;

/**
 * @class Repository Repository
 *
 * @package Selene\Package\Cms
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class Repository
{
    protected $events;
    protected $extensions;

    /**
     * Constructor.
     *
     * @param DispatcherInterface $events
     */
    public function __construct(DispatcherInterface $events)
    {
        $this->events = $events;
        $this->extensions = [];
    }

    /**
     * addExtension
     *
     * @param ExtensionInterface $ext
     *
     * @return void
     */
    public function addExtension(ExtensionInterface $ext)
    {
        if ($this->hasExtension($name = $ext->getAlias())) {
            throw new \InvalidArgumentException(
                sprintf('Extension "%s" already registered or duplicate extension name.', $name)
            );
        }

        $this->extensions[$name] = $ext;
    }

    /**
     * getExtension
     *
     * @param mixed $name
     *
     * @return ExtensionInterface|null
     */
    public function getExtension($name)
    {
        return $this->hasExtension($name) ? $this->extensions[$name] : null;
    }

    /**
     * hasExtension
     *
     * @param string $name
     *
     * @return boolean
     */
    public function hasExtension($name)
    {
        return isset($this->extensions[$name]);
    }

    public function uninstall(ExtensionInterface $ext)
    {
        // do stuff
        $ext->tearDown();
    }

    public function install(ExtensionInterface $ext)
    {
    }

    public function enable(ExtensionInterface $ext)
    {
    }

    public function disable(ExtensionInterface $ext)
    {
    }

    /**
     * boot
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    public function boot(ApplicationInterface $app)
    {
        foreach ((array)$this->extensions as $extension) {
            if ($this->isEnabled($extension)) {
                $extension->boot($app);
            }
        }
    }

    protected function isInstalled(ExtensionInterface $ext)
    {
        return true;
    }

    protected function isEnabled(ExtensionInterface $ext)
    {
        return true;
    }
}
