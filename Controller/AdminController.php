<?php

/**
 * This File is part of the Selene\Package\Cms\Controller package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms\Controller;

use \Selene\Package\Framework\Controller\Controller;

/**
 * @class AdminController extends Controller
 * @see Controller
 *
 * @package Selene\Package\Cms\Controller
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 * @license MIT
 */
class AdminController extends Controller
{
    /**
     * indexAction
     *
     * @return string
     */
    public function indexAction()
    {
        $this->ok($this->render('cms:content:index.twig', ['admin' => 'test']));
    }
}
