<?php

/**
 * This File is part of the Selene\Package\Cms package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */

namespace Selene\Package\Cms;

use \Selene\Module\Package\Package;
use \Selene\Adapter\Kernel\ApplicationInterface;
use \Selene\Module\DI\BuilderInterface;
use \Selene\Module\DI\ContainerInterface;
use \Selene\Module\DI\Processor\ProcessorInterface;
use \Selene\Package\Cms\Process\CollectPackageMap;

/**
 * @class CmsPackage
 * @package Selene\Package\Cms
 * @version $Id$
 */
class CmsPackage extends Package
{
    /**
     * {@inheritdoc}
     */
    public function requires()
    {
        return ['framework', 'twig'];
    }

    public function build(BuilderInterface $builder)
    {
        $builder->getProcessor()
            ->add(new CollectPackageMap, ProcessorInterface::AFTER_REMOVE);
    }

    /**
     * boot
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    public function boot(ApplicationInterface $app)
    {
        $this->registerExtensionWrapper($app->getContainer());
        $this->bootExtensions($app);
    }

    /**
     * bootExtensions
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    private function bootExtensions(ApplicationInterface $app)
    {
        $app->getContainer()->get('cms.extension_repository')->boot($app);
    }

    /**
     * registerExtensionWrapper
     *
     * @param ApplicationInterface $app
     *
     * @return void
     */
    private function registerExtensionWrapper(ContainerInterface $container)
    {
        if (false === stream_wrapper_register('extension', $class = __NAMESPACE__.'\Filesystem\ResourceStream')) {
            throw new \RuntimeException('Cannot register resource stream.');
        }

        $class::setMapInstantiator(function () use ($container) {
            return $container->getParameter('extension.resources');
        });
    }
}
